---
layout: event_detail
title: Collaborative Working Sessions - Documentation + Tooling
event: venice2022
order: 50
permalink: /events/venice2022/documentation+tooling/
---

Reproducible Builds Summit 2022

Notes
-----

Goal: bring a developer from tool to the right point of the documentation. ie: from diffoscope to [https://reproducible-builds.org/docs/timestamps/](https://reproducible-builds.org/docs/timestamps/])

Let's start exploring how we could do that with diffoscope.

How could we have rule-based matching? Text matching could be simple, but it doesn't identify all cases of variations. For example, it doesn't identify sorting problems.

A more powerful approach is to have Turing-complete matchers: you invoke /usr/lib/diffoscope/rules/*.rule DIFF-RESULT.JSON and look at the exit code.

graphical visualizations can help: before running diffoscope, tell the user what are the common problems through checklists for every platform.
Also a flowchart could help:
 * are you doing this? yes/no
 * are you using platform XYZ? yes/no

Debate about pre (checklists) or post (troubleshooting).

The goal of this meetings needs to be redefined.
Goal Proposal: having a better representation of the information that is already included in /docs/.

Having pages for major language / platforms / buildsystems. Those pages should include:
 * Checklists: thinkgs you should always do
   * set variable blah
   * make sure you are (or aren't using) option --foo=bar
   * make sure you are using at least version 1.2.3, except version 1.3.4 which we know it's buggy (because link/to/bug/tracker/666)
 * Common problems: problems that you can expect to find, and how to solve them
 * Success stories on that language! (link)

The other approach (troubleshooting) can list solutions (snippets) for different language / platforms / buildsystems. They can meet each other as a filter/matrix of taksks + what it is useful for + languages it applies to


Redefining the goal once again: let's _add_ new content in /docs/, which is more practical than the very high-level doc we currently have, includes copy-paste and clear actions that a developer can make.

We could call most of the current doc "the R-B book". What we want to add is "R-B by examples".

Those pages can be aligned hierarchical, but hierarchical is not enough: for example, binutils applies not only to the "C" category but to everything using its linker (for example, when mixing Rust and C), so also want tags.

Let's make an example of the informational architecture we want to have, and one example of one of those language-specific page we'd like to have.

Proposal 1: let's split the doc into Book and HowTo.
This is not always clear: where should the introduction go? Also, some part of the current doc could go into HowTo, but only small bits.

Proposal 2:
 * Why/
   * Intro
   * Buy-In
 * Book/
   * < 99% of current "Achieve deterministic builds" >
 * HowTo have R-B when using/
   * C
     * CMake
     * automake
     * plain makefile
   * Python
     * wheel
   * Rust
   * Go
   * JVM
 * Howto fix this specific problem/
   * timestamp-in-ELF
   * zip file order

How happy are we of that?
The current plan is a good idea, but it needs more feedback.



